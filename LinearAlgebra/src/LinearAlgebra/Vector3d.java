/*
* Parhomenco Kirill 1933830
* Sorry for the confusion with lab directory and package name
* */

package LinearAlgebra;
import java.lang.*;
import java.util.Objects;

public class Vector3d {
    private double x, y ,z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }
//    to get magnitude of the current vector
    public double magnitude(){
        return Math.sqrt(getX() * getX() + getY() * getY() + getZ() * getZ());
    }
//    dot product of two given vectors
    public double dotProduct(Vector3d givenVector){
        return (getX()* givenVector.getX()+
                getY()*givenVector.getY()+
                getZ()* givenVector.getZ());

    }
//    Adding two vectors
    public Vector3d add(Vector3d givenVector){
        Vector3d newVector = new Vector3d(getX()+ givenVector.getX(),
                getY() + givenVector.getY(),
                getZ()+ givenVector.getZ());
        return newVector;
    }

//    Override because why not
    public String toString() {
        return "Vector3d{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    @Override
    /*
    * So about this... I am sorry Daniel
    * I was trying to assert equals when putting an object to the expected field
    * Of course it did not work so I discovered that it uses equals() Object method to check that
    * So in this case even if the contents are the same, the address is different so it shows that test has not passed
    * I kinda cheated a bit wit the code below but I took my time to actually try to understand what each line means
    * I would love to get some feedback as i was happy to override something else than toString
    * */
    public boolean equals(Object o) {
        if (this == o) return true; // in case if it is actually the same exact object
        if (o == null || getClass() != o.getClass()) return false;
        /*
        null verification,
        second part of that made me go and read about getClass() method
        so it checks if the current class is the same class as the o passed to the method
        basically checking if we are comparing onions to onions and not to cucumbers
        hope i got it right
        */
        Vector3d vector3d = (Vector3d) o;
//        that's new, had to lookup ways to create new object without the 'new' keyword
//        there is plenty, this is not creating an object, it is actually copying (creating copy of reference?)
//        of the object o into object vector3d

        return Double.compare(vector3d.x, x) == 0 &&
                Double.compare(vector3d.y, y) == 0 &&
                Double.compare(vector3d.z, z) == 0;
//        well, this is just comparing doubles, nothing complicated
    }


}
