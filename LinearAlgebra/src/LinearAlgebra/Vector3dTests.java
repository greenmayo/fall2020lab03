/*
* Parhomenco Kirill 1933830
* */
package LinearAlgebra;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Vector3dTests {
    Vector3d guineapig = new Vector3d(1,2,3);
    Vector3d otherguineapig = new Vector3d(4,5,6);

    @Test
    void magnitude() {
        assertEquals(3.74165738, guineapig.magnitude(), 0.0000001);
        assertEquals(8.77496438, otherguineapig.magnitude(), 0.0000001);
    }

    @Test
    void dotProduct() {
        assertEquals(32, guineapig.dotProduct(otherguineapig));
    }

    @Test
    void add() {
        Vector3d resultguineapig = new Vector3d(5,7,9);
        assertEquals(resultguineapig, guineapig.add(otherguineapig));
    }

    @Test
    void getX() {
        assertEquals(1,guineapig.getX());
    }

    @Test
    void getY() {
        assertEquals(2,guineapig.getY());
    }

    @Test
    void getZ() {
        assertEquals(3,guineapig.getZ());
    }
}